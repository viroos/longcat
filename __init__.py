import badgelink
import badgenet
import audio
import socket
from select import poll, POLLIN
from st3m.application import Application, ApplicationContext
    

class Longcat(Application):


    def __init__(self, app_ctx: ApplicationContext) -> None:

        badgenet.configure_jack(badgenet.SIDE_LEFT, badgenet.MODE_ENABLE_AUTO)
        badgenet.configure_jack(badgenet.SIDE_RIGHT, badgenet.MODE_ENABLE_AUTO)
        self.addr = f"ff02::1%{badgenet.get_interface().name()}"
        self.sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
        self.sock.bind((self.addr, 2137))
        self.number = 0
        self.time_since_last_broadcast = 0
        self.poll = poll()
        self.poll.register(self.sock, POLLIN)
        try:
            self.bundle_path = app_ctx.bundle_path
        except Exception:
            self.bundle_path = "/flash/sys/apps/viroos-longcat"

        super().__init__(app_ctx)   

    def detect_cat_part(self):
        if self.number == 0:
            return "whole_cat"
        elif self.number == 1:
            return "head"
        elif self.number == 9001:
            return "tail"
        else:
            return "torso"

    def draw(self, ctx: Context) -> None:
        # Clear screen
        ctx.rgb(255, 255, 255).rectangle(-120, -120, 240, 240).fill()

        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE

        cat_part = self.detect_cat_part()
        theframe = f"{self.bundle_path}/img/{cat_part}.png"
        ctx.image(theframe,-100,-100,200,200)


       
    def think(self, ins: InputState, delta_ms: int) -> None:
        if not badgelink.left.active() and audio.headphones_are_connected():
            badgelink.left.enable()
        if not badgelink.right.active() and audio.line_in_is_connected():
            badgelink.right.enable()
        if not (audio.headphones_are_connected() or audio.line_in_is_connected()):
            self.number = 0
            return
        self.time_since_last_broadcast += delta_ms
        if self.time_since_last_broadcast > 100:
            self.time_since_last_broadcast -= 100
            self.sock.sendto(str(self.number), (self.addr, 2137))

        for s, _ in self.poll.poll(1):
            data, addr = s.recvfrom(1024)
            switch_table = badgenet.get_switch_table()
            switch_ports = {
                switch_port_name: {
                    mac
                    for mac, switch_port, _ in switch_table
                    if switch_port_name == switch_port and mac.startswith("34:85:18")
                }
                for switch_port_name in ["left", "right", "local"]
            }


            if switch_ports["left"] is not None:
                self.number = len(switch_ports["left"])+1
            
            if not audio.line_in_is_connected():
                self.number = 9001
