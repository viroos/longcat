# Longcat Display for Flow3r Badges

## Overview

This is a Python application for the Flow3r hacker camp badge/development board. The application is designed to display a long cat image across multiple connected badges, creating a long chain display effect.

## Functionality

The application detects the position of the badge in the chain and displays the corresponding part of the long cat image on the badge screen. Depending on the position, it can show the head, torso, or tail of the cat. This way, multiple badges connected together will show a whole long cat image, creating an entertaining visual.

## Requirements

- This application runs on the Flow3r Badge, which must be equipped with two audio ports (left and right).

## Usage

To run the Longcat application:

1. Install the app Flow3r Badge.
2. Always connect the right jack of one badge to the left jack of the other badge. Other connection methods are not supported.
3. Run the application, and the badge will display the corresponding part of the long cat image.

Important note: The badge disconnection detection is based on jack port state. If you want to remove a badge from the chain, you must fully disconnect both ends of the cable. Dangling (hanging) cables can cause errors.

## Code Explanation

The code is structured as follows:

1. Configures both left and right audio jacks
2. Creates a socket and binds it to a certain address and port
3. Registers this socket to the poll object for monitoring
4. In the `detect_cat_part()` method, it determines which part of the cat (head, torso, or tail) to display based on the badge position in the chain
5. In the `draw()` method, it displays the respective part of the cat on the badge screen
6. In the `think()` method, again, badge position in the chain is processed and sent to other badges

## License

This project is released under the MIT License 

## Attribution

Special thanks to Mewp whose code in the project Gard3n was used in this project too.

